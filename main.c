#include <stdlib.h>
#include <stdio.h>
#include <string.h>

// Cheesy Zip file parser
// 1) PArse each file Entry
//    - print name of file
//    - print uncompressed file size
//    = print compressed file size
// 2) Parse each Central Directory Record
//    - print file name
//    - print file compressed size
//    - print file uncompressed size
// 3) Parse end of Central Directory
//    - print number of records
//
//  Use spec @ pkware.cachefly.net/webdocs/casestudies/APPNOTE.txt
//

// Note. x86 is LE, zipfile spec states all value are LE - We get to cheat here
#define LFH_MAGIC   0x04034B50
#define CFH_MAGIC   0x02014B50
#define ECD_MAGIC   0x06054B50

struct  __attribute__ ((packed)) LFH
{
    // MAGIC gets read separately, so that 4-byte field is omitted
    unsigned char padd[14];
    unsigned int compressed_size;
    unsigned int uncompressed_size;
    unsigned short filename_length;
    unsigned short extra_field_length;
};

struct __attribute__ ((packed)) CFH
{
    // MAGIC gets read separately, so that 4-byte field is omitted
    unsigned char padd[16];
    unsigned int compressed_size;
    unsigned int uncompressed_size;
    unsigned short filename_length;
    unsigned short extra_field_length;
    unsigned short comment_length;
    unsigned char padd2[12];
};

struct __attribute__ ((packed)) ECDR
{
    // MAGIC gets read separately, so that 4-byte field is omitted
    unsigned char padd[6];
    unsigned short total_entries;
    unsigned char padd2[10];
};

/**
 * parse one local file header
 * @param buffer  Pointer to buffer ( heopfully start of LFH record)
 * @return The size in bytes of this record or 0 on error
 */
unsigned
print_local_file_header(FILE *f)
{
    struct LFH thisLFH;
    puts("--------------------------------");
    puts("Local File Header (Partial)");
    puts("--------------------------------");
    int r = fread(&thisLFH, 1, sizeof(struct LFH), f);
    if( r != sizeof(struct LFH))
    {
        printf("Failed to read local file HEADER");
        return 0;
    }
    printf("Compressed Size : %d\n", thisLFH.compressed_size);
    printf("Uncompressed Size : %d\n", thisLFH.uncompressed_size);
    int fnl = (int) thisLFH.filename_length;
    printf("filename length == %u\n", fnl);
    char *fn_buffer = (char *) calloc(fnl + 1, sizeof(char));
    if (fn_buffer == NULL)
    {
        puts("Malloc Failed\n");
        return 0;
    }
    else
    {
        if(fread(fn_buffer, 1, fnl, f) != fnl)
        {
            puts("Failed to read file name");
            return 0;
        }
        printf("FileName : %s\n", fn_buffer);
        free(fn_buffer);
        // skip the extra field if any
        fseek(f,thisLFH.extra_field_length + thisLFH.compressed_size, SEEK_CUR);
    }
    return 1;
}

/**
 * parse one local file header
 * @param buffer  Pointer to buffer ( heopfully start of LFH record)
 * @return The size in bytes of this record or 0 on error
 */
unsigned
print_central_file_header(FILE *f)
{
    struct CFH thisCFH;
    puts("--------------------------------");
    puts("Central File Header (Partial)");
    puts("--------------------------------");
    int r = fread(&thisCFH, 1, sizeof(struct CFH), f);
    if( r != sizeof(struct CFH))
    {
        printf("Failed to read local file HEADER");
        return 0;
    }
    printf("Compressed Size : %d\n", thisCFH.compressed_size);
    printf("Uncompressed Size : %d\n", thisCFH.uncompressed_size);
    int fnl = (int) thisCFH.filename_length;
    printf("filename length == %u\n", fnl);
    char *fn_buffer = (char *) calloc(fnl + 1, sizeof(char));
    if (fn_buffer == NULL)
    {
        puts("Malloc Failed\n");
        return 0;
    }
    else
    {
        if(fread(fn_buffer, 1, fnl, f) != fnl)
        {
            puts("Failed to read file name");
            return 0;
        }
        printf("FileName : %s\n", fn_buffer);
        free(fn_buffer);
        // skip the extra field if any
        fseek(f,thisCFH.extra_field_length + thisCFH.comment_length, SEEK_CUR);
    }
    return 1;
}
int
print_end_of_central_directory_record(FILE * f)
{
    struct ECDR thisECDR;
    puts("--------------------------------");
    puts("End Of Central Directory Record (Partial)");
    puts("--------------------------------");
    int r = fread(&thisECDR, 1, sizeof(struct ECDR), f);
    if( r != sizeof(struct ECDR))
    {
        printf("Failed to read END OF CENTRAL DIRECTORY RECORD");
        return 0;
    }
    printf("Total Records : %u\n", thisECDR.total_entries);
    // This is ignoring any comments in the ZIP FILE Comment field.
    return 1;
}

int main()
{
    unsigned int magic;
    FILE *f = fopen("/home/user/temp/test.zip", "rb");

    while(1)
    {
        if (fread(&magic, 1, sizeof(unsigned int), f) != sizeof(unsigned int))
        {
            printf("NOPE\n");
            exit(1);
        }

        switch (magic)
        {
            case LFH_MAGIC:
            {
                if (print_local_file_header(f) == 0)
                {
                    puts("Error Printing Local File Header");
                    exit(1);
                }
                break;
            }
            case CFH_MAGIC:
            {
                if( print_central_file_header(f) == 0)
                {
                    puts("Error Printing Central File Header");
                    exit(1);
                }
                break;
            }
            case ECD_MAGIC:
            {
                if(print_end_of_central_directory_record(f) == 9)
                {
                    puts("Error Printing End of Central Directory Record");
                    exit(1);
                }
                puts("End of Central Directory");
            }
            default:
            {
                puts("BEATS ME");
                fclose(f);
                exit(1);
            }
        }
    }
}
